function nextStep(stepNumber) {
    const currentStep = document.querySelector('.form-step.active');
    currentStep.classList.remove('active');

    const nextStep = document.getElementById('step' + stepNumber);
    if (nextStep) {
        nextStep.classList.add('active');
    } else {
        alert('No more steps!');
    }
}

function submitForm() {
    // Here you would add the actual submission logic
    alert('Form Submitted! Collect data and process as needed.');
}
