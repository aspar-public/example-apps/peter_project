from transformers import AutoModelForCausalLM, AutoTokenizer


def download_model(model_name):
    # This will download the model and the tokenizer and save them in a directory with the model's name
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModelForCausalLM.from_pretrained(model_name)

    # Save the model and the tokenizer to your local machine
    model.save_pretrained(f"./{model_name}")
    tokenizer.save_pretrained(f"./{model_name}")


if __name__ == "__main__":
    model_name = "INSAIT-Institute/BgGPT-7B-Instruct-v0.2"  # Replace with the model you want
    download_model(model_name)
