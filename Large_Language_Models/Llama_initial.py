from transformers import AutoModelForCausalLM, AutoTokenizer


def main():
    model_name = "Model-Name"  # Replace with the actual model name
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModelForCausalLM.from_pretrained(model_name)

    prompt = "The future of AI is"
    input_ids = tokenizer.encode(prompt, return_tensors="pt")

    # Generate text using Beam Search
    beam_output = model.generate(
        input_ids,
        max_length=50,
        num_beams=5,
        early_stopping=True
    )
    print("Beam Search Output:", tokenizer.decode(beam_output[0], skip_special_tokens=True))

    # Generate text using Top-K Sampling
    top_k_output = model.generate(
        input_ids,
        max_length=50,
        do_sample=True,  # Enable sampling
        top_k=50
    )
    print("Top-K Sampling Output:", tokenizer.decode(top_k_output[0], skip_special_tokens=True))


if __name__ == "__main__":
    main()
