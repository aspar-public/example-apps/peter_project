# Import necessary libraries

from transformers import AutoTokenizer, AutoModelForSequenceClassification, TrainingArguments, Trainer
from datasets import load_dataset
from sklearn.metrics import accuracy_score, precision_recall_fscore_support


# Function to tokenize the dataset
def tokenize_function(examples):
    # Tokenize the texts and return the result
    return tokenizer(examples["text"], padding="max_length", truncation=True, max_length=512)


# Function to compute metrics for evaluation
def compute_metrics(eval_pred):
    logits, labels = eval_pred
    predictions = logits.argmax(axis=-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, predictions, average='binary')
    acc = accuracy_score(labels, predictions)
    return {"accuracy": acc, "f1": f1, "precision": precision, "recall": recall}


# Step 1: Load the dataset
dataset = load_dataset("imdb")

# Step 2: Load tokenizer
model_name = "./distilgpt2"  # Using local DistilGPT2 model
tokenizer = AutoTokenizer.from_pretrained(model_name)

# Set the padding token if it's not already set
if tokenizer.pad_token is None:
    tokenizer.pad_token = tokenizer.eos_token

# Step 3: Tokenize the dataset
tokenized_datasets = dataset.map(tokenize_function, batched=True)

# Step 4: Load a pre-trained model for sequence classification
model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=2)

# Step 5: Define training arguments
training_args = TrainingArguments(
    output_dir="results",  # Directory to store model checkpoints
    num_train_epochs=3,  # Number of epochs for training
    per_device_train_batch_size=1,  # Batch size for training (set to 1)
    per_device_eval_batch_size=1,  # Batch size for evaluation (set to 1)
    warmup_steps=500,  # Number of warmup steps
    weight_decay=0.01,  # Weight decay for regularization
    logging_dir='./logs',  # Directory to store logs
    evaluation_strategy="epoch",  # Evaluate after each epoch
    save_strategy="epoch",  # Save the model after each epoch
    load_best_model_at_end=True,  # Load the best model at the end of training
)
# Step 6: Initialize the Trainer
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_datasets["train"],
    eval_dataset=tokenized_datasets["test"],
    compute_metrics=compute_metrics,  # Pass the compute_metrics function
)

# Step 7: Start training
trainer.train()

# Step 8: Evaluate the model
eval_results = trainer.evaluate()
print(eval_results)

# Step 9: Save the model and tokenizer
trainer.save_model("./final_model")
tokenizer.save_pretrained("./final_model")
